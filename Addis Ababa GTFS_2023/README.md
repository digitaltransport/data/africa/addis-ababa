# Addis Ababa 2023

* Datasets collected by [AddisMapTransit](https://addismaptransit.com/) through a financial and technical support from [DT4A Innovation Challenge](https://digitaltransport4africa.org/innovation-challenge/).

* The data was collected from August, 2022 to March, 2024 and entered into OpenStreetMap (OSM). The GTFS was then extracted from OSM.

* The agencies are contained in 3 separate GTFS files, which include
  * Anbessa City Bus Service Enterprise `et-addisababa-bus-ab.zip` - 910 stops and 122 routes
  * Sheger Mass Transport Service Enterprise `et-addisbaba-bus-sh.zip` - 501 stops and 72 routes
  * Addis Ababa Transport Authority - for the paratransit taxis (minibus) `et-addisbaba-minibus.zip` - 905 stops and 249 routes
    
The is also a file `et-addisababa-bus.zip` with all bus trips and `et-addisbaba.zip` containing all trips (busses and minibusses).

Data version 1.0.0 mirrored from https://github.com/AddisMap/AddisMapTransit-gtfs/releases/tag/1.0.0

## License

The data is published under the same license as OpenStreetMap (OdBL). 
