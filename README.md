# Addis Ababa

GTFS datasets for Addis Ababa city, Ethiopia.

• [Addis Ababa GTFS_2018](https://gitlab.com/digitaltransport/data/africa/addis-ababa/-/tree/master/Addis%20Ababa%20GTFS_2018?ref_type=heads) contains a metadata file and GTFS datasets collected by World Resources Institute (WRI) in 2018. 

• [Addis Ababa GTFS_2023](https://gitlab.com/digitaltransport/data/africa/addis-ababa/-/tree/master/Addis%20Ababa%20GTFS_2023) contains a metadata file and GTFS datasets collected by [AddisMapTransit](https://addismaptransit.com/) through a financial and technical support from [DT4A Innovation Challenge](https://digitaltransport4africa.org/innovation-challenge/). 